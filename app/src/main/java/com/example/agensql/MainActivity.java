package com.example.agensql;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import database.AgendaContacto;
import database.Contacto;

public class MainActivity extends AppCompatActivity {
    private EditText edtNombre;
    private EditText edtTelefono1;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto savedContact;

    private int id;
    private AgendaContacto db;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono1 = (EditText) findViewById(R.id.edtTell);
        edtTelefono2 = (EditText) findViewById(R.id.edtTel2);
        edtDireccion = (EditText) findViewById(R.id.edtDireccion);
        edtNotas = (EditText) findViewById(R.id.edtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);

        db = new AgendaContacto(MainActivity.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtNombre.getText().toString().equals("") || edtDireccion.getText().toString().equals("")
                        || edtTelefono1.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "",
                            Toast.LENGTH_SHORT).show();
                } else {

                    Contacto nContacto = new Contacto();
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setTelefono1(edtTelefono1.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setDireccion(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());

                    if (cbxFavorito.isChecked()) {
                        nContacto.setFavorite(1);
                    } else {
                        nContacto.setFavorite(0);
                    }

                    db.openDatabse();

                    if (savedContact == null) {
                        long idx = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this, "Se agrego el contacto con ID: " + idx, Toast.LENGTH_SHORT).show();
                    } else {
                        db.actualizarContacto(nContacto, id);
                        Toast.makeText(MainActivity.this, "Se Actualizo el registro: " + id, Toast.LENGTH_SHORT).show();
                    }

                    db.cerrar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                startActivityForResult(intent, 0);
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNombre.getText().toString().equals("") || edtDireccion.getText().toString().equals("")
                        || edtTelefono1.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "",
                            Toast.LENGTH_SHORT).show();
                } else {
                    limpiar();
                }
            }
        });

    }

    public void limpiar() {
        savedContact = null;
        edtNombre.setText("");
        edtTelefono1.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (Activity.RESULT_OK == resultCode) {
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = (int) contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtTelefono1.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtDireccion.setText(contacto.getDireccion());
            edtNotas.setText(contacto.getNotas());
            if (contacto.getFavorite() > 0) {
                cbxFavorito.setChecked(true);
            } else {
                limpiar();
            }
        }
    }

}
